package ratestest.com.fx.service;

public class RouterClass {

	public String routingManager(String buyorSell,String baseCurrencyCode,String baseCurrencyAmt,String contraCode,String contraAmount,String receivingAmount   
			) {
		String result = null;

		if ((baseCurrencyAmt != null) && (contraAmount !=null) )   {

			result = "error 1005-025";
		}
		else if (receivingAmount != null) {
			if ((buyorSell.equals("Buy")) && (baseCurrencyCode.equals("EUR")) && (baseCurrencyAmt.equals("200"))
					&& (contraCode.equals("USD")) && (receivingAmount.equals("EFT"))) {

				result = "buy";
			} else if ((buyorSell.equals("Sell")) && (baseCurrencyCode.equals("GBP")) && (baseCurrencyAmt.equals("100"))
					&& (contraCode.equals("USD")) && (receivingAmount.equals("EFT"))) {

				result = "sell";
			} else {
				result = "error 1005-998";

			}
		} else if (receivingAmount == null) {
			if ((buyorSell.equals("Buy")) && (baseCurrencyCode.equals("EUR")) && (baseCurrencyAmt.equals("200"))
					&& (contraCode.equals("USD"))) {

				result = "buy";
			} else if ((buyorSell.equals("Sell")) && (baseCurrencyCode.equals("GBP")) && (baseCurrencyAmt.equals("100"))
					&& (contraCode.equals("USD"))) {

				result = "sell";
			} else {
				result = "error 1005-998";

			}
		}
		return result;
	}

}
